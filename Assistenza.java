import java.util.Scanner;

public class Assistenza {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String emailAssistenza = "Amarbleit@gmail.com";
        String numeroTelefono = "+390471431129";

        System.out.println("Benvenuto nel servizio di assistenza di Amarble.");
        System.out.println("Scegli come desideri ricevere assistenza:");
        System.out.println("1. Assistenza via email");
        System.out.println("2. Assistenza via numero di cellulare");

        int scelta = scanner.nextInt();

        if (scelta == 1) {
            System.out.println("Assistenza via email: " + emailAssistenza);
        } else if (scelta == 2) {
            System.out.println("Assistenza via numero di cellulare: " + numeroTelefono);
        } else {
            System.out.println("Scelta non valida. Per favore, scegli 1 o 2.");
        }
    }
}

