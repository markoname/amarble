import java.util.Scanner;
public class Azienda {
  String nome; 
  String indirizzo;
  String contatto;
  String email;
  String sito;
  String citta;
  String responsabile;
  Negozio n1;
  Negozio n2; 
  Negozio n3;
  Negozio n4;
  public Azienda(String nome, String indirizzo, String citta, String contatto, String email, String responsabile, String sito){
    this.nome = nome;
    this.indirizzo = indirizzo;
    this.contatto = contatto;
    this.email = email;
    this.sito = "marbletekbussiness.com";
    
     this.n1 = new Negozio("Amarble", "Via Garibaldi 33","Roma", "0331 476 908", "instoremarmble@bussiness.com", "Amadou Sow", this.sito);
     this.n2 = new Negozio("Amarble", "Via Milano 67","Bolzano", "2435 345 795", "inStoreMarmble@bussiness.com", "Bledar Balanca", this.sito);
     this.n3 = new Negozio("Amarble", "Via Alessandro Manzoni 8", "Milano", "6876 334 532", "inStoreMarmble@bussiness.com", "Marko Rabrenovic", this.sito);
     this.n4 = new Negozio("Amarble", "Via Giuseppe Bologna 23", "Firenze", "6543 445 981", "inStoreMarmble@bussiness.com"," Cristian Marin", this.sito);
 
  }

  public void negozioUnoInfo(){
     System.out.println("Nome: " + n1.nome);
     System.out.println("Indirizzo: " + n1.indirizzo);
     System.out.println("Contatto: " + n1.contatto);
     System.out.println("Email: " + n1.email);
     System.out.println("Sito: " + n1.sito);
     
   }
  public void negozioDueInfo(){
     System.out.println("Nome: " + n2.nome);
     System.out.println("Indirizzo: " + n2.indirizzo);
     System.out.println("Contatto: " + n2.contatto);
     System.out.println("Email: " + n2.email);
     System.out.println("Sito: " + n2.sito);
   }
  public void negozioTreInfo(){
     System.out.println("Nome: " + n3.nome);
     System.out.println("Indirizzo: " + n3.indirizzo);
     System.out.println("Contatto: " + n3.contatto);
     System.out.println("Email: " + n3.email);
     System.out.println("Sito: " + n3.sito);
   }
  public void negozioQuattroInfo(){
     System.out.println("Nome: " + n4.nome);
     System.out.println("Indirizzo: " + n4.indirizzo);
     System.out.println("Contatto: " + n4.contatto);
     System.out.println("Email: " + n4.email);
     System.out.println("Sito: " + n4.sito);

   }

  public void negozioVicino(){
      Scanner scanner = new Scanner(System.in);
     System.out.println("Inserisca le proprie coordinate per essere indirizzato al nostro punto vendita più vicino a lei.");
     System.out.println("Latitudine: ");
    double latitudine = scanner.nextDouble();
     System.out.println("Longitudine: ");
    double longitudine= scanner.nextDouble();
  Navigatore navigatore = new Navigatore(latitudine, longitudine);
  System.out.println(navigatore.trovaCittaPiuVicina()) ;
  }
  
  }









